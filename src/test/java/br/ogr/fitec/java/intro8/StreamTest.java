package br.ogr.fitec.java.intro8;

import br.org.fitec.java.intro8.stream.SnowPatrol;
import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by jgodoi on 24/10/2016.
 */
public class StreamTest {

    private List<String> lyric;

    @Before
    public void setUp() throws Exception {
        lyric = SnowPatrol.callOutInTheDark();
    }

    @Test
    public void creatingStreamsExamples() throws Exception {
        System.out.println(lyric);

        Stream<String> lyricStream = lyric.stream();
        System.out.println(lyricStream);

        Stream<String> lyricParallelStream = lyric.parallelStream();
        System.out.println(lyricParallelStream);

        Stream<String> lyricStreamFromArray = Stream.of(lyric.get(0),lyric.get(1),lyric.get(2));
        System.out.println(lyricStreamFromArray);

        Stream<String> empty = Stream.empty();
        System.out.println(empty);
    }

    @Test
    public void forEachExample() throws Exception {
        lyric.stream().forEach(line -> System.out.println(line));
    }

    @Test
    public void filterExample() throws Exception {
        lyric.stream().filter(line -> !line.isEmpty()).forEach(line -> System.out.println(line));
    }

    @Test
    public void mapExample() throws Exception {
        lyric.stream().map(line -> line.toUpperCase()).forEach(line -> System.out.println(line));
    }

    @Test
    public void sortExample() throws Exception {
        lyric.stream().sorted().forEach(line -> System.out.println(line));

        lyric.stream().sorted(Comparator.reverseOrder()).forEach(line -> System.out.println(line));
    }
}
