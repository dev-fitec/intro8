package br.ogr.fitec.java.intro8.challenges;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;

import java.io.FileReader;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by jgodoi on 26/10/2016.
 */
public class Challenge5Test {


    private Stream<Map.Entry<String, JsonElement>> bitcoinPriceIndex;

    @Before
    public void setUp() throws Exception {
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(new FileReader("./src/test/resources/bitcoin.json")).getAsJsonObject();
        bitcoinPriceIndex = jsonObject.get("bpi").getAsJsonObject().entrySet().stream();
    }
}
