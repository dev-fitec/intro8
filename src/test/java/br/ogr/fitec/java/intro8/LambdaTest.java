package br.ogr.fitec.java.intro8;

import br.org.fitec.java.intro8.functional.Alchemist;
import br.org.fitec.java.intro8.functional.Magician;
import br.org.fitec.java.intro8.functional.Priester;
import br.org.fitec.java.intro8.functional.Summoner;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jgodoi on 20/10/2016.
 */
public class LambdaTest {

    @Test
    public void lambdaExample() {
        Magician black = () -> {
            System.out.println("Abra Kadabra!");
        };
        black.doSomeMagic();

        Alchemist ed = (a, b) -> {
            return a + b;
        };

        Alchemist al = (a, b) -> {
            return b + a;
        };
        Assert.assertEquals("It couldn't be combined", "penapple", ed.combine("pen", "apple"));
        Assert.assertEquals("It couldn't be combined", "pinapplepen", al.combine("pen","pinapple"));
    }

    @Test
    public void hardLambdaExample(){
        Summoner vladislav = (magician) -> { return (component1, component2) -> {
            magician.doSomeMagic();
            return component1+component2;
        };
        };

        System.out.println(
                vladislav.summon(
                        ()->System.out.println("I have ...")
                ).combine("penapple","pinapplepen")
        );
    }

    @Test
    public void beyond7thSenseExample(){
        Priester seyia = (constelation -> {
            constelation.doSomeMagic();
            return (attack, armor) -> armor+" "+attack;
        });
        System.out.println(seyia.sayYourPrayer());
        System.out.println(
                seyia.summon(
                        ()-> System.out.println("Gimme your strength!")
                ).combine("Meteor","Pegasus"));
    }
}
