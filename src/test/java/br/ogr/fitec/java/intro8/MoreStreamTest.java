package br.ogr.fitec.java.intro8;

import br.org.fitec.java.intro8.ThereIsNotEnoughPower;
import br.org.fitec.java.intro8.stream.more.rpg.players.Player;
import br.org.fitec.java.intro8.stream.more.rpg.units.Healer;
import br.org.fitec.java.intro8.stream.more.rpg.units.Hero;
import br.org.fitec.java.intro8.stream.more.rpg.units.Peon;
import br.org.fitec.java.intro8.stream.more.rpg.units.Unit;
import br.org.fitec.java.intro8.stream.more.rpg.units.factories.UnitFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by jgodoi on 24/10/2016.
 */
public class MoreStreamTest {
    private List<Unit> units;
    private Player player;
    private UnitFactory unitFactory;

    @Before
    public void setUp() throws Exception {
        player = new Player();
        unitFactory = new UnitFactory(player.getResources());
        units = new ArrayList<>();
        units.addAll(unitFactory.getPeons(10));
        units.addAll(unitFactory.getHealers(5));
        units.addAll(unitFactory.getKnights(10));
    }

    @Test
    public void primitiveAndWrapperExamples() throws Exception {
        Integer one = 1;
        Assert.assertTrue("It ain't an object!", one instanceof Object);

        int onePrimitive = one;

        //If you uncomment the next line this won't compile
        //Assert.assertFalse("It shouldn't be an object!", onePrimitive instanceof Object);

    }

    @Test
    public void mapToIntExample() throws Exception {
        Stream<Integer> integerStream = units.stream().map(unit -> unit.getConsume());

        IntStream intStream = units.stream().mapToInt(unit -> unit.getConsume());
    }

    @Test
    public void reductionsExample() throws Exception {
        OptionalInt max = units.stream().mapToInt(unit -> unit.getAttack()).max();

        OptionalInt min = units.stream().mapToInt(unit -> unit.getAttack()).min();

        int sum = units.stream().mapToInt(unit -> unit.getAttack()).sum();

        long count = units.stream().count();

        OptionalDouble average = units.stream().mapToInt(unit -> unit.getAttack()).average();
    }

    @Test
    public void reduceMethodExamples() throws Exception {
        int totalAttack = units.stream().mapToInt(unit -> unit.getAttack()).reduce(0, (combined, attack) -> combined + attack);

        Integer reduce = units.stream().reduce(0, (result, unit) -> result + unit.getAttack(), (combined, actual) -> combined + actual);

        Assert.assertTrue("Are sure that is not equal?", totalAttack==reduce);
    }

    @Test
    public void summaryStatisticsExample() throws Exception {
        IntSummaryStatistics summaryStatistics = units.stream().mapToInt(unit -> unit.getAttack()).summaryStatistics();
        System.out.println(summaryStatistics);
    }

    @Test
    public void findMethodExamples() throws Exception {
        Optional<Unit> firstPeon = units.stream().filter(u -> u instanceof Peon).findFirst();

        Optional<Unit> anyAlive = units.parallelStream().filter(u -> u.getEnergy() > 0).findAny();
    }

    @Test
    public void matchMethodExamples() throws Exception {
        boolean allMatch = units.stream().allMatch(unit -> unit instanceof Unit);

        boolean anyMatch = units.stream().anyMatch(unit -> unit instanceof Healer);

        boolean noneMatch = units.stream().noneMatch(unit -> unit instanceof Hero);
    }

    @Test
    public void optionalMethodExamples() throws Exception {
        Unit firstPeon = units.stream().filter(u -> u instanceof Peon).findFirst().get();

        units.stream().mapToInt(unit -> unit.getAttack()).average().ifPresent(av -> System.out.println(av));

        System.out.println(units.stream().mapToInt(unit -> unit.getSpecialMovements().size()).average().isPresent());

        units.stream().mapToInt(unit -> unit.getSpecialMovements().size()).average().orElse(0);

        units.stream().mapToInt(unit -> unit.getSpecialMovements().size()).average().
                orElseGet(()-> new Random().doubles(10).average().getAsDouble());

        try {
            units.stream().filter(unit -> unit.getAttack() > 100).findAny()
                    .orElseThrow(() -> new ThereIsNotEnoughPower("Run Forest, Run!"));
        } catch (ThereIsNotEnoughPower weak){
            System.out.println(weak.getMessage());
        }
    }

    @Test
    public void limitAndSkipExample() throws Exception {
        units.stream().forEach(unit -> System.out.println(unit));

        Stream<Unit> saveTheFirstThree = units.stream().skip(10);
        saveTheFirstThree.limit(5).forEach(unit -> System.out.println(unit));
    }

    @Test
    public void distinctExample() throws Exception {
        units.stream().map(unit -> unit.getClass().getSimpleName()).distinct().forEach(name -> System.out.println(name));
    }

    @Test
    public void referenceMethodExamples() throws Exception {
        //calling a method from each instance
        double averageSpeed = units.stream().mapToInt(Unit::getSpeed).average().getAsDouble();

        //receiving each instance as argument
        units.stream().forEach(System.out::println);
    }

    @Test
    public void collectorsExample() throws Exception {
        units.stream().filter(unit -> unit.getAttack() > 0).collect(Collectors.toList());
    }

    @Test
    public void groupingByExample() throws Exception {
        Map<String, List<Unit>> unitsByType = units.stream().collect(Collectors.groupingBy(unit -> unit.getClass().getSimpleName()));
        System.out.println(unitsByType);
    }

    @Test
    public void partitioninByExample() throws Exception {
        Map<Boolean, List<Unit>> strongers = units.stream().collect(Collectors.partitioningBy(unit -> unit.getAttack() >= 10));
        System.out.println(strongers.get(true));
        System.out.println(strongers.get(false));
    }

    @Test
    public void comparatorExample() throws Exception {
        System.out.println(units);

        units.sort(Comparator.comparing(Unit::getAttack));
        System.out.println(units);

        units.sort(Comparator.comparing(Unit::getAttack).thenComparing(unit -> unit.getClass().getSimpleName()));
    }
}
