package br.ogr.fitec.java.intro8;

import br.org.fitec.java.intro8.review.Auto;
import br.org.fitec.java.intro8.review.Chassi;
import br.org.fitec.java.intro8.review.Estatue;
import br.org.fitec.java.intro8.review.Ninja;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by jgodoi on 17/10/2016.
 */
public class ReviewTest {
    @Test
    public void creatingAnonymousClassFromAuto(){
        Auto auto = new Auto() {

            public void move() {
                ///
            }

            public void stop() {
                //
            }

            public void speedUp() {
                ///
            }

            public void slowDown() {
                //
            }

            public void shiftGear() {
                //
            }
        };

        Assert.assertNotNull("Auto not instanced", auto);
    }

    @Test
    public void creatingAnonymousClassFromChassi(){
        Chassi chassi = new Chassi() {
            public String getSerialNumber() {
                return "serial number";
            }
        };
        Assert.assertEquals("Problem to get serial number from chassi", "serial number", chassi.getSerialNumber());
    }

    @Test
    public void creatingAnonymousClassFromRunner() throws Exception {
        Runnable target = new Runnable() {
            public void run() {
                System.out.println("I'm in another thread.");
            }
        };
        new Thread(target).start();
    }

    @Test
    public void staticalExamples() throws Exception {
        Estatue.main("");
        System.out.println(Estatue.move);
        System.out.println((new Estatue()).move);
        //System.out.println((new Estatue()).main(""));
        //System.out.println(Estatue.move=1);
        System.out.println(Estatue.position=1);//FIXME what is wrong?
    }

    @Test
    public void reflectionExamples() throws Exception {
        System.out.println(this.getClass().getSimpleName());
        System.out.println(Ninja.class.newInstance());
        for(Method method: Ninja.class.getMethods())
            System.out.println(method);
        for(Field field: Ninja.class.getFields())
            System.out.println(field);
    }

}
