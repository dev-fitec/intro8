package br.ogr.fitec.java.intro8;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jgodoi on 20/10/2016.
 */
public class CollectionTest {
    @Test
    public void forEachExample() throws Exception {
        List<String> todoList = Arrays.asList("clean the garage",
                "wash the dishes",
                "take the garbage out",
                "fix holes in my shoes",
                "change the jeans",
                "tell Howard to get his jumbo jet off my airport");

        todoList.forEach(task -> System.out.println(task));
    }

    @Test
    @Ignore
    //FIXME what's wrong?
    public void removeIfExample() throws Exception {

        List<String> todoList = Arrays.asList("clean the garage",
                "wash the dishes",
                "take the garbage out",
                "fix holes in my shoes",
                "change the jeans",
                "tell Howard to get his jumbo jet off my airport");

        todoList.removeIf((s)->s.contains("wash the dishes"));

        todoList.forEach(task -> System.out.println(task));
    }

    @Test
    public void sortExample() throws Exception {

        List<String> todoList = Arrays.asList("clean the garage",
                "wash the dishes",
                "take the garbage out",
                "fix holes in my shoes",
                "change the jeans",
                "tell Howard to get his jumbo jet off my airport");

        todoList.sort((o1, o2) -> o1.charAt(0)-o2.charAt(0));

        todoList.forEach(task -> System.out.println(task));

    }

    @Test
    public void replaceAllExample() throws Exception {

        List<String> todoList = Arrays.asList("clean the garage",
                "wash the dishes",
                "take the garbage out",
                "fix holes in my shoes",
                "change the jeans",
                "tell Howard to get his jumbo jet off my airport");

        todoList.replaceAll((s -> s+" done!"));

        todoList.forEach(task -> System.out.println(task));
    }
}
