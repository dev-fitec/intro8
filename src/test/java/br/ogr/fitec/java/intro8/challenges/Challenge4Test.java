package br.ogr.fitec.java.intro8.challenges;

import org.junit.Before;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jgodoi on 24/10/2016.
 */
public class Challenge4Test {
    private List<String> plantsAndWeeds;

    @Before
    public void setUp() throws Exception {
        plantsAndWeeds = Arrays.asList("Camellias 7", "weed 5", "Sunflower 3", "Lily 1", "weed 4", "Azalea 5", "weed 2", "Begonia 4");
    }
}
