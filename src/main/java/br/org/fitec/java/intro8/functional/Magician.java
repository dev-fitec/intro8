package br.org.fitec.java.intro8.functional;

/**
 * Created by jgodoi on 19/10/2016.
 */
@FunctionalInterface
public interface Magician {
    void doSomeMagic();
}
