package br.org.fitec.java.intro8.functional;

/**
 * Created by jgodoi on 20/10/2016.
 */
public interface Priester extends Summoner{
    default String sayYourPrayer(){
        return "Saoriiiiiiiii!";
    }
}
