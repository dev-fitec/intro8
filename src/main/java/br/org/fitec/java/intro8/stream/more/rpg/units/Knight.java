package br.org.fitec.java.intro8.stream.more.rpg.units;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Knight extends Unit {
    public Knight(){
        this.setAttack(10);
        this.setEnergy(30);
        this.setActualEnergy(30);
        this.setSpeed(10);
        this.setConsume(3);
    }
}
