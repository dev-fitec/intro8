package br.org.fitec.java.intro8.stream.more.rpg.units;

import br.org.fitec.java.intro8.stream.more.rpg.magics.Heal;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Healer extends Unit {
    public Healer (){
        this.setAttack(2);
        this.setEnergy(10);
        this.setActualEnergy(10);
        this.setSpeed(1);
        this.setConsume(1);
        this.addSpecialMovement(new Heal(2));
    }
}
