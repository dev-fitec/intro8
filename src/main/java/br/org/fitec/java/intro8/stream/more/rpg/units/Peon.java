package br.org.fitec.java.intro8.stream.more.rpg.units;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Peon extends Unit{
    public Peon (){
        this.setAttack(4);
        this.setEnergy(15);
        this.setActualEnergy(15);
        this.setSpeed(1);
        this.setConsume(1);
    }
}
