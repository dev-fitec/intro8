package br.org.fitec.java.intro8.stream.more.rpg.units;

import br.org.fitec.java.intro8.stream.more.rpg.magics.SpecialMovement;

import java.util.*;

/**
 * Created by jgodoi on 25/10/2016.
 */
public abstract class Unit {
    private int attack = 0;
    private int energy = 0;
    private int actualEnergy = 0;
    private int speed = 0;
    private int consume = 0;
    private List<SpecialMovement> specialMovements = new ArrayList<>();

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getActualEnergy() {
        return actualEnergy;
    }

    public void setActualEnergy(int actualEnergy) {
        this.actualEnergy = actualEnergy;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getConsume() {
        return consume;
    }

    public void setConsume(int consume) {
        this.consume = consume;
    }

    public List<SpecialMovement> getSpecialMovements() {
        return Collections.unmodifiableList(specialMovements);
    }

    public void addSpecialMovement(SpecialMovement movement){
        this.specialMovements.add(movement);
    }
}
