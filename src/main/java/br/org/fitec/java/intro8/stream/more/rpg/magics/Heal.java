package br.org.fitec.java.intro8.stream.more.rpg.magics;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Heal extends SpecialMovement {
    private int canHeal;
    public Heal(){
        this.canHeal = 0;
    }

    public Heal(int canHeal) {
        this.canHeal = canHeal;
    }
}
