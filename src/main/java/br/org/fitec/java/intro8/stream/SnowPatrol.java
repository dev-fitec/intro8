package br.org.fitec.java.intro8.stream;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jgodoi on 24/10/2016.
 */
public class SnowPatrol {
    public static List<String> callOutInTheDark() {
        return Arrays.asList("It's like we just can't help ourselves",
                "'Cause we don't know how to back down",
                "We were called out to the streets",
                "We were called out into the towns",
                "",
                "And how the heavens, they opened up",
                "Like arms of dazzling Gold",
                "With our rainwashed histories",
                "Well they do not need to be told",
                "",
                "Show me now, show me the arms aloft",
                "Every eye trained on a different star",
                "This magic",
                "This drunken semaphore",
                "I",
                "",
                "We are listening and",
                "We're not blind",
                "This is your life, this is your time",
                "",
                "We are listening and",
                "We're not blind",
                "This is your life, this is your time",
                "",
                "I was called out in the dark",
                "By a choir of beautiful cheats",
                "And as the kids took back the parks",
                "You and I were left with the streets",
                "",
                "Show me now, show me the arms aloft",
                "Every eye trained on a different star",
                "This magic",
                "This drunken semaphore",
                "And I",
                "",
                "We are listening and",
                "We're not blind",
                "This is your life, this is your time",
                "",
                "We are listening and",
                "We're not blind",
                "This is your life, this is your time",
                "",
                "We are listening and",
                "We're not blind",
                "This is your life, this is your time",
                "",
                "We are listening and",
                "We're not blind",
                "This is your life, this is your time");
    }
}
