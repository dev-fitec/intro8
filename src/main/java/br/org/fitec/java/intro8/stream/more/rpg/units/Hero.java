package br.org.fitec.java.intro8.stream.more.rpg.units;

import br.org.fitec.java.intro8.stream.more.rpg.magics.MagicAttack;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Hero extends Unit {
    public Hero () {
        this.setAttack(15);
        this.setEnergy(100);
        this.setActualEnergy(100);
        this.setSpeed(5);
        this.setConsume(10);
        this.addSpecialMovement(new MagicAttack(30));
    }
}
