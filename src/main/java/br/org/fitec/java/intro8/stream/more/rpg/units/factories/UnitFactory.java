package br.org.fitec.java.intro8.stream.more.rpg.units.factories;

import br.org.fitec.java.intro8.stream.more.rpg.players.Resources;
import br.org.fitec.java.intro8.stream.more.rpg.units.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class UnitFactory {

    private final Resources playerResources;

    public UnitFactory(Resources playerResources) {
        this.playerResources = playerResources;
    }

    public static UnitFactory instance(Resources playerResources){
        return new UnitFactory(playerResources);
    }

    public List<Peon> getPeons(int quantity){
        return this.createUnits(Peon.class,quantity);
    }

    public List<Healer> getHealers(int quantity){
        return this.createUnits(Healer.class,quantity);
    }

    public List<Knight> getKnights(int quantity) {
        return this.createUnits(Knight.class,quantity);
    }

    public List<Hero> getHero(int quantity){
        return this.createUnits(Hero.class,quantity);
    }

    private <U extends Unit> List<U> createUnits(Class<U> clazz, int quantity){
        ArrayList<U> peons = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            try {
                U unit = clazz.newInstance();
                if(playerResources.getAvailableFood() > unit.getConsume()) {
                    playerResources.setAvailableFood(playerResources.getAvailableFood() - unit.getConsume());
                } else {
                    throw new OutOfAvailableFood("You can't produce more" + clazz.getSimpleName());
                }
                peons.add(unit);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return peons;
    }
}
