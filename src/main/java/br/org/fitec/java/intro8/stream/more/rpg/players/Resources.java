package br.org.fitec.java.intro8.stream.more.rpg.players;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Resources {
    private int foodQuantity = 100;
    private int availableFood = 100;

    public int getFoodQuantity() {
        return foodQuantity;
    }

    public void setFoodQuantity(int foodQuantity) {
        this.foodQuantity = foodQuantity;
    }

    public int getAvailableFood() {
        return availableFood;
    }

    public void setAvailableFood(int availableFood) {
        this.availableFood = availableFood;
    }
}
