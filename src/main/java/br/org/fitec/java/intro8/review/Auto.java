package br.org.fitec.java.intro8.review;

/**
 * Created by jgodoi on 17/10/2016.
 */
public interface Auto {
    void move();
    void stop();
    void speedUp();
    void slowDown();
    void shiftGear();
}
