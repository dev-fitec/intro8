package br.org.fitec.java.intro8;

/**
 * Created by jgodoi on 26/10/2016.
 */
public class ThereIsNotEnoughPower extends RuntimeException {
    public ThereIsNotEnoughPower(String message) {
        super(message);
    }
}
