package br.org.fitec.java.intro8.stream.more.rpg.units.factories;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class OutOfAvailableFood extends RuntimeException {
    public OutOfAvailableFood(String message) {
        super(message);
    }
}
