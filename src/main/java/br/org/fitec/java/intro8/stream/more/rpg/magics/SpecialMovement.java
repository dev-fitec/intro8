package br.org.fitec.java.intro8.stream.more.rpg.magics;

/**
 * Created by jgodoi on 25/10/2016.
 */
public abstract class SpecialMovement {
    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }
}
