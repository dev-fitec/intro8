package br.org.fitec.java.intro8.stream.more.rpg.players;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class Player {

    private Resources resources;

    public Player() {
        this.setResources(new Resources());
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }
}
