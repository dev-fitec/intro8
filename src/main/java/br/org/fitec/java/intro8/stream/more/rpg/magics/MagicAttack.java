package br.org.fitec.java.intro8.stream.more.rpg.magics;

/**
 * Created by jgodoi on 25/10/2016.
 */
public class MagicAttack extends SpecialMovement {
    private int damage;

    public MagicAttack(){
        this.damage =0;
    }

    public MagicAttack(int damage) {
        this.damage=damage;
    }

    public int getDamage() {
        return damage;
    }
}
