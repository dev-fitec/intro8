package br.org.fitec.java.intro8.review;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jgodoi on 19/10/2016.
 */
public class Ninja {
    private List<String> tools= new ArrayList<>();
    public final String KONOHA = "Konoha";

    public void disappear(){
        System.out.println("puff");
    }
    protected String getVillage(){
        return KONOHA;
    }

    private List<String> getJutsus(){
        return Arrays.asList("Kage Bushin", "Rasengan");
    }
}
