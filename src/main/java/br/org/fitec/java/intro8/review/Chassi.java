package br.org.fitec.java.intro8.review;

/**
 * Created by jgodoi on 17/10/2016.
 */
public abstract class Chassi {
    public abstract String getSerialNumber();
}
