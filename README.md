### What is this repository for? ###

* Training about Java 8 lambda and stream features

### Where is the slides for this material? ###

* https://docs.google.com/presentation/d/1LOJgdnTj61o6gCanjaKBpwsRfl6IorFLlNkRBHslz0w/edit?usp=sharing

### How do I get set up? ###

* https://docs.google.com/presentation/d/118Lm39wrTyua4xLx3jCCQym26KR2HuvcvIGbHqdjj-8/edit?usp=sharing